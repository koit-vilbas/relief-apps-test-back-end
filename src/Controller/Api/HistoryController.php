<?php

namespace App\Controller\Api;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\History;

class HistoryController extends AbstractController
{
    /**
     * @Route("/history", name="history", methods={"GET"})
     */
    public function list()
    {
        $entries = $this->getDoctrine()
            ->getRepository(History::class)
            ->findAll();

        $mappedEntries = [];
        foreach ($entries as $entry) {
            $mappedEntries[] = $entry->getUrl();
        }

        $mappedEntries = array_reverse($mappedEntries);

        return $this->json($mappedEntries);
    }

    /**
     * @Route("/history", name="history.store", methods={"POST"})
     * @param EntityManagerInterface $entityManager
     * @param Request                $request
     *
     * @return bool|JsonResponse
     */
    public function store(EntityManagerInterface $entityManager, Request $request) {
        $data = json_decode($request->getContent());

        if(!empty($data->url)) {
            $historyEntry = new History();
            $historyEntry->setUrl($data->url);

            $entityManager->persist($historyEntry);

            $entityManager->flush();
            return $this->json(true);
        }
        return $this->json(false);
    }
}
